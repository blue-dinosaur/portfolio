//=require "vendor/lazysizes-min.js"


jQuery(document).ready(function($) {


    // ===== HERO AREA
    // ================================================================

    // Once the hero image is fully loaded, add a class to the parent
    var heroInterval = setInterval(function () {
        var hero_image = document.querySelector('.hero-image');
        if (hero_image.complete) {
            clearInterval(heroInterval);
            // Start the hero animation
            $('.hero').addClass('loaded');
            // Remove the loading icon
            setTimeout(function() {
                $('.hero .loading').remove();
            }, 1000);
        }
    }, 250);


    // ===== GENERAL PAGE SCRIPTING
    // ================================================================

    var app = {

        settings: {
            dismissable: $('.dismissable span')
        },

        init: function() {
            this.bindUIActions();
        },

        bindUIActions: function() {
            // Dismissable message buttons
            this.settings.dismissable.on('click', function() {
                $(this).parent().fadeOut('slow');
            });
        }

    };
    app.init();


    // ===== CLIENT EXPERIENCE SECTION
    // ================================================================

    var clients = {

        settings: {
            stage: $('.client-projects-stage'),
            slider: $('.client-projects-stage .slider'),
            slider_options: {
                controls: false,
                pager: false,
                maxSlides: 1,
                nextSelector: $('.client-projects-stage .next'),
                prevSelector: $('.client-projects-stage .prev'),
                responsive: true,
                touchEnabled: false
            },
            thumbnails: $('.client-thumbnails .thumbnail')
        },

        init: function() {
            this.bxInit();
            this.gatherStats();
            this.bindUIActions();
        },

        bxInit: function() {
            this.settings.slider.bxSlider(this.settings.slider_options);
            this.settings.slider_options.prevSelector.on('click', function() {
                clients.settings.slider.goToPrevSlide();
                clients.gatherStats();
            });
            this.settings.slider_options.nextSelector.on('click', function() {
                clients.settings.slider.goToNextSlide();
                clients.gatherStats();
            });
        },

        bindUIActions: function() {
            this.settings.thumbnails.on('click', clients.clientNavigation);
        },

        // Control the slider and opening/closing of the stage
        clientNavigation: function() {
            // Clear the delay just in case
            clearTimeout(delay);
            // The thumbnail selected
            var index = parseInt($(this).attr('data-client-link'));

            // If the stage is not open, add a delay to the slider while scrolling
            var timer = (clients.settings.stage.hasClass('open')) ? 0 : 500;

            var delay = setTimeout(function() {
                clients.settings.stage.addClass('open');
                // Fade in slider
                setTimeout(function() {
                    clients.settings.slider.fadeTo('fast', 1);
                }, 100);
            }, timer);

            // Then smooth scroll to the content
            $('html, body').animate({
                scrollTop: clients.settings.stage.offset().top
            });

            clients.settings.slider.goToSlide(index);
            clients.gatherStats();

        },

        // Gather stats on the current slide and pass it along
        gatherStats: function() {

            var index = this.settings.slider.getCurrentSlide();
            var current_slide = $('[data-slide="' + index + '"]');

            // Add a class to the current slide for stat animation
            if (!current_slide.hasClass('viewed')) {
                current_slide.addClass('viewed');

                // Delayed start
                setTimeout(function() {
                    // Get all available stats to animate
                    var stats = $(current_slide.find('[data-stat]'));
                    // Loop over each on a delay
                    stats.each(function() {
                        var current_stat = $(this)[0];
                        var end_value = this.dataset.stat;
                        // Send it off to the animation function
                        clients.animateStats(current_stat, end_value);
                    });
                }, 1000);

            }
        },

        animateStats: function(current, end) {
            var number = 0;
            // Number of steps to animate
            var steps = 10;
            // Set the amount to increment
            // Set the decimal places to 1 if the number is a floating point value
            if (end % 1 != 0) {
                var increment = +((end / steps).toFixed(1));
            } else {
                var increment = Math.floor(end / steps);
            }

            // For smaller numbers, just increment by one
            if (end < steps) {
                var increment = 1;
            }

            // Loop based on the increment
            var interval = setInterval(animate, 60);

            function animate() {
                number = +((number + increment).toFixed(1));

                if (number < end) {
                    current.innerHTML = number;
                } else {
                    clearInterval(interval);
                    current.innerHTML = end;
                }
            }
        }

    }
    clients.init();


    // ===== PERSONAL PROJECTS SECTION
    // ================================================================

    var projects = {

        settings: {
            slider: $('.personal-projects .slider'),
            slider_options: {
                controls: false,
                infiniteLoop: true,
                maxSlides: 1,
                pager: true,
                responsive: true,
                slideMargin: 100,
                touchEnabled: true
            }
        },

        init: function() {
            this.bxInit();
            this.bindUIActions();
        },

        bxInit: function() {
            this.settings.slider.bxSlider(this.settings.slider_options);
        },

        bindUIActions: function() { }

    }
    projects.init();


    // ===== CONTACT FORM SECTION
    // ================================================================

    // Forms and controlling labels animating up and down
    var contact = {

        settings: {
            contact_section: $('.contact'),
            form: $('.contact-form'),
            form_rows: $('.form-row input, .form-row textarea')
        },

        init: function() {
            if (this.settings.form_rows.length) {
                this.bindUIActions();
                this.initializeLabels();
            }
        },

        bindUIActions: function() {
            // Form engage!
            this.settings.form.on('click', contact.engageForm);
            // Click events on .form-rows
            this.settings.form_rows.on('focus', contact.activateLabel);
            this.settings.form_rows.on('blur', contact.checkLabels);
        },

        // Engage form
        engageForm: function() {
            contact.settings.contact_section.addClass('open');
        },

        // Apply active class to the one in focus
        activateLabel: function() {
            $(this).siblings('label').addClass('active');
        },

        // Check for empty inputs, and remove class
        checkLabels: function() {
            contact.settings.form_rows.each(function () {
                if ($(this).val() == '') {
                    $(this).siblings('label').removeClass('active');
                }
            });
        },

        // On page load check for input values and add active classes to those
        initializeLabels: function() {
            contact.settings.form_rows.each(function() {
                if ($(this).val() != '') {
                    $(this).siblings('label').addClass('active');
                }
            });
        }

    };
    contact.init();


});