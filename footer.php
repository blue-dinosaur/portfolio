<footer class="footer">
    <div class="wrapper">
        <div class="social-icons">
            <a class="icon" href="https://bitbucket.org/blue-dinosaur/" target="_blank"><?=get_svg('icon-social-bitbucket')?></a>
            <a class="icon" href="https://github.com/dstrong462" target="_blank"><?=get_svg('icon-social-github')?></a>
            <a class="icon" href="https://www.linkedin.com/in/daniel-strong-0105b5126" target="_blank"><?=get_svg('icon-social-linkedin')?></a>
            <a class="icon" href="#contact"><?=get_svg('icon-social-mail')?></a>
        </div>
        <div class="maze">
            <img src="library/svg/maze.svg" alt="Maze">
        </div>
        <div class="copyright">
            Design &amp; Development by<br/>
            Daniel Strong &copy;<?=date('Y')?>
        </div>
    </div>

</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="//cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script async type="text/javascript" src="library/js/min/scripts-min.js?v=<?=filemtime('library/js/min/scripts-min.js')?>"></script>

</body>
</html>
