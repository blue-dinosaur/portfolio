<?php include('includes/config.php'); ?>
<?php include('header.php'); ?>

<section class="hero">
    <div class="loading"></div>
    <div class="hero-wrapper">
        <img class="hero-image" src="library/svg/hero.svg" alt="Isometric Landscape">
        <img class="hero-clouds-front" src="library/svg/hero-clouds-front.svg" alt="Isometric Clouds">
        <img class="hero-clouds-back" src="library/svg/hero-clouds-back.svg" alt="Isometric Clouds">
        <img class="hero-name" src="library/svg/hero-name.svg" alt="Daniel Strong">
    </div>
</section>

<section class="tools">
    <div class="wrapper">
        <h1 class="title">About Me</h1>
        <div class="column about">
            <div class="about-text animate from-left bounce lazyload" data-expand="-100"><span>Hi</span>I'm Daniel, a front-end developer currently working in the Raleigh-Durham area. I'm passionate about learning new technologies while working constantly to improve my existing skillset. Below you'll find details about tools that I'm familiar with, a detailed list of clients that I have experience with, as well as a few personal projects.</div>
            <img src="library/svg/desk.svg" class="tools-image animate from-left bounce lazyload" alt="Me at my Desk" data-expand="-100">
        </div>
        <div class="column tools">
            <div class="skills-block animate from-right bounce lazyload" data-expand="-100">
                <ul class="tools-list">
                    <?php foreach ($web_tools as $key => $val) : ?>
                    <li>
                        <img src="library/svg/icon-<?=$val?>.svg" alt="<?=$key?>">
                        <div class="text"><?=$key?></div>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <ul class="tools-list mobile animate from-right bounce lazyload" data-expand="-100">
                <li class="title">Mobile Development</li>
                <?php foreach ($mobile_tools as $key => $val) : ?>
                <li>
                    <img src="library/svg/icon-<?=$val?>.svg" alt="<?=$key?>">
                    <div><?=$key?></div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</section>

<section class="client-projects-stage open">
    <div class="wrapper">
        <ul class="slider">
        <?php $index = 0; ?>
        <?php foreach ($clients as $client) : ?>
            <li class="slide" data-slide="<?=$index?>">
                <div class="content">
                    <h2 class="title"><?=$client['title']?></h2>
                    <p class="subtitle"><?=$client['subtitle']?></p>
                    <div class="description">
                        <?php foreach ($client['description'] as $description) { ?>
                        <p><?=$description?></p>
                        <?php } ?>
                    </div>
                    <ul class="ul services checkmarks">
                        <?php foreach ($client['services'] as $service) { ?>
                            <li><?=$service?></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="stats">
                    <div class="stats-container">
                        <?php foreach ($client['stats'] as $key => $val) { ?>
                        <div class="stat">
                            <?php $value = (is_array($val) ? $val['value'] : $val); ?>
                            <?php $unit = (is_array($val) ? $val['unit'] : ''); ?>
                            <?php $currency = (is_array($val) ? $val['currency'] : ''); ?>
                            <div class="value <?=$unit?> <?=$currency?>" data-stat="<?=$value?>">0</div>
                            <div class="text"><?=$key?></div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php if (isset($client['link'])) { ?>
                    <a class="btn" href="<?=$client['link']?>" target="_blank">Visit Site</a>
                    <?php } ?>
                </div>
            </li>
            <?php $index++; ?>
        <?php endforeach; ?>
        </ul>
        <div class="prev"><?=get_svg('icon-chevron')?></div>
        <div class="next"><?=get_svg('icon-chevron')?></div>
    </div>
</section>

<section class="client-projects-nav">
    <h1 class="title">Client Experience</h1>
    <div class="wrapper">
        <div class="client-thumbnails">
        <?php $index = 0; ?>
        <?php foreach ($clients as $client) : ?>
            <div class="thumbnail" data-client-link="<?=$index?>">
                <img src="library/svg/<?=$client['logo']?>" alt="<?=$client['title']?>">
            </div>
            <?php $index++; ?>
        <?php endforeach; ?>
        </div>
    </div>
    <div class="message dismissable">Click logos for more info<span>x</span></div>
</section>

<section class="personal-projects">
    <h1 class="title">Personal Projects</h1>
    <div class="wrapper">
        <ul class="slider">

            <?php // NUMBER KNIGHT ?>
            <li class="slide">
                <div class="content animate from-left lazyload" data-expand="-100">
                    <h2 class="title">Number Knight</h2>
                    <div class="description">
                        <p>Number Knight is a math RPG (exciting, right?) developed entirely with CSS and vanilla JavaScript.</p>
                        <p>Each dungeon features randomized traps, columns for cover, enemies, and math problems. Test your skills with 3 different difficulty levels for both Math and Monsters.</p>
                        <ul class="ul checkmarks">
                            <li>6 Game Modes</li>
                            <li>3 Boss Fights</li>
                            <li>6 Enemies types with different abilities</li>
                        </ul>
                    </div>
                    <div class="btn-row">
                        <a class="btn-icon" href="https://github.com/dstrong462/numberknight" target="_blank"><?=get_svg('icon-social-github')?></a>
                        <a class="btn-icon" href="https://play.google.com/store/apps/details?id=com.vermiliongiant.numberknight" target="_blank"><?=get_svg('icon-google-play')?></a>
                    </div>
                </div>
                <div class="image animate from-right lazyload" data-expand="-100">
                    <img src="library/img/project-number-knight.gif" alt="Number Knight">
                </div>
            </li>

            <?php // COLOR MATCH ?>
            <li class="slide">
                <div class="content animate from-left lazyload" data-expand="-100">
                    <h2 class="title">Color Match</h2>
                    <div class="description">
                        <p>Test your color matching accuracy by adding and subtracting red, green, and blue to match the target color. The closer you get, the more points you'll receive!</p>
                        <p>This is the first game that I made with JavaScript, but I had a blast making it. I learned a ton of cool new things that can be done with JavaScript, how to store and access data with localStorage, and best of all is that now I have a fun little game to play if I get bored.</p>
                    </div>
                    <div class="btn-row">
                        <a class="btn-icon" href="https://github.com/dstrong462/color-match" target="_blank"><?=get_svg('icon-social-github')?></a>
                        <a class="btn-icon" href="https://play.google.com/store/apps/details?id=com.vermiliongiant" target="_blank"><?=get_svg('icon-google-play')?></a>
                    </div>
                </div>
                <div class="image animate from-right lazyload" data-expand="-100">
                    <img src="library/img/project-color-match.png" alt="Color Match">
                </div>
            </li>

        <ul>
    </div>
</section>

<a name="contact"></a>
<section class="contact <?=(empty($form['errors']) ? '' : 'open')?>">
    <div class="wrapper">
        <?php if ($_GET['thank_you']) : ?>
        <div class="thanks">
            <h1>Thank You!</h1>
            <p>Your form has been successfully submitted, and I will respond to you shortly. Check your inbox for a copy of your form submission.</p>
            <div class="smiley"><?=get_svg('icon-smile')?></div>
            <div class="smiley"><?=get_svg('icon-smile')?></div>
        </div>
        <?php else : ?>
        <form class="contact-form" action="#contact" method="post">
            <h1 class="title">Get in Touch</h1>
            <?php if (!empty($form['errors'])) : ?>
            <ul class="errors">
            <?php foreach ($form['errors'] as $error) { ?>
                <li class="error"><?=$error?></li>
            <?php } ?>
            </ul>
            <?php endif; ?>
            <div class="form-row">
                <label for="user_name" class="required">Name</label>
                <input type="text" id="user_name" name="user_name" value="<?=$form['input']['user_name']?>">
            </div>
            <div class="form-row">
                <label for="user_email" class="required">Email</label>
                <input type="email" id="user_email" name="user_email" value="<?=$form['input']['user_email']?>">
            </div>
            <div class="form-row">
                <label for="user_comments">Comments</label>
                <textarea name="user_comments" id="user_comments" rows="6"><?=$form['input']['user_comments']?></textarea>
            </div>
            <input type="hidden" id="breathing" name="breathing">
            <button class="btn" type="submit" name="submit">submit</button>
        </form>
        <?php endif; ?>
    </div>
</section>

<?php include('footer.php'); ?>
