<?php

    // List of all tools as 'Display Name' => 'SVG Name'
    $web_tools = array (
        'HTML5' => 'html5',
        'CSS3' => 'css3',
        'Sass' => 'sass',
        'JavaScript' => 'js',
        'jQuery' => 'jquery',
        'Git' => 'git',
        'Gulp' => 'gulp',
        'VSCode' => 'vscode',
        'AMP' => 'amp',
        'PHP' => 'php',
        'WordPress' => 'wordpress',
        'Drupal' => 'drupal',
        'MySQL' => 'mysql',
        'Illustrator' => 'illustrator',
        'Photoshop' => 'photoshop',
        'InDesign' => 'indesign',
        'Flash' => 'flash',
        'Slack' => 'slack'
    );

    // List of all mobile tools as 'Display Name' => 'SVG Name'
    $mobile_tools = array(
        'Apache Cordova' => 'cordova',
        'Adobe PhoneGap' => 'phonegap',
        'Google Play' => 'google-play'
    );

    // Client data
    $clients = array(

        array(
            'title' => 'Window World',
            'subtitle' => 'Window World: America\'s Largest Exterior Remodeler',
            'description' => array(
                'In less than 20 years, Window World grew from a small, local business to America\'s largest exterior remodeler.',
                'From new feature additions, and full site reskins to Google Analytics and call tracking installation, I\'ve done it all. With such a large collection of sites to work with, and owners with ever changing needs there is always something new and challenging to learn.'
            ),
            'services' => array('Landing Page Development', 'Full Site Reskins', 'Site Updates'),
            'logo' => 'logo-client-window-world.svg',
            'stats' => array(
                'Total Sites' => 306,
                'Windows Sold' => array(
                    'value' => 15,
                    'unit' => 'million'
                )
            ),
            'link' => 'https://www.windowworldatlanta.com/free-estimate/'
        ),

        array(
            'title' => 'Kohler<sup>&reg;</sup>',
            'subtitle' => 'The World\'s #1 Hydrotherapy Company',
            'description' => array(
                'Kohler<sup>&reg;</sup> is one of the world\'s leading kitchen, bath, and plumbing manufacturers in the world.',
                'They came to us for their development needs on a brand new microsite to highlight the features of their line of Walk-In Bath products. I helped develop a prototype page, and performed site updates to improve SEO, and overall site functionality.'
            ),
            'services' => array('Microsite Development', 'Site Updates'),
            'logo' => 'logo-client-kohler.svg',
            'stats' => array(
                'Annual Revenue' => array(
                    'value' => 5.9,
                    'unit' => 'billion',
                    'currency' => 'usd'
                ),
                'Employees' => array(
                    'value' => 35,
                    'unit' => 'thousand'
                )
            ),
            'link' => 'https://www.kohlerwalkinbath.com'
        ),

        array(
            'title' => 'Seegars Fence Company',
            'subtitle' => 'Providing fence installations that develop lifetime relationships',
            'description' => array(
                'With 17 locations Seegars Fence Company is one of the Southeast\'s largest residential and commercial fencing companies.',
                'This was a full site built from the ground up to bring their existing web presence to the next level.'
            ),
            'services' => array('Full Site Development', 'WordPress', 'Geolocation'),
            'logo' => 'logo-client-seegars.svg',
            'stats' => array(
                'Locations' => 17,
                'Years of Service' => date('Y') - 1949
            ),
            'link' => 'https://www.seegarsfence.com'
        ),

        array(
            'title' => 'Blue Heron Signature Homes',
            'subtitle' => 'Inspiring Homes. Beyond Expectations.',
            'description' => array(
                'Blue Heron is a home builder that focuses on constructing high-end custom homes.',
                'This microsite was designed in-house, and developed from scratch to showcase their expanding catalog of high-end homes.'
            ),
            'services' => array('Microsite Development'),
            'logo' => 'logo-client-blue-heron.svg',
            'stats' => array(
                'Homesite Locations' => 4,
                'Avg Home Price' => array(
                    'value' => 1.3,
                    'unit' => 'million',
                    'currency' => 'usd'
                )
            ),
            'link' => 'https://custom.blueheronhomesnc.com'
        ),

        array(
            'title' => 'Animal Emergency Hospital &amp; Urgent Care',
            'subtitle' => 'Over 35 Years of Trusted Veterinary Care',
            'description' => array(
                'Animal Emergency Hospital &amp; Urgent Care is a 24/7 full service veterinary and surgical center in Raleigh.',
                'This full site was designed in-house, and developed from the ground up to replace their existing full site. It uses WordPress as its back end.'
            ),
            'services' => array('Full Site Development', 'WordPress'),
            'logo' => 'logo-client-animal-emergency.svg',
            'stats' => array(
                'Established' => 1979
            ),
            'link' => 'https://www.ervets4pets.com'
        ),

        array(
            'title' => 'Bailey\'s Fine Jewelry',
            'subtitle' => 'Where The Triangle Gets Engaged',
            'description' => array(
                'Bailey\'s is a premium jewelry store with multiple locations in North Carolina. It has been providing high-end jewelry for over 70 years.',
                'I\'ve worked on site updates, adding new landing pages, and new features throughout the site.'
            ),
            'services' => array('Landing Page Development', 'Site Updates'),
            'logo' => 'logo-client-baileys.svg',
            'stats' => array(
                'Locations' => 4,
                'Featured Designers' => 26
            ),
            'link' => 'https://www.baileybox.com'
        ),

        array(
            'title' => 'Golf Pride',
            'subtitle' => 'The #1 grip on tour',
            'description' => array(
                'Golf Pride has been the global leader in golf grip innovation and technology for over 60 years. Today, Golf Pride boasts 80 percent usage at virtually every level of major, professional, and amateur competition; and you’ll find grips made by Golf Pride on nearly every new club sold today. Simply put, Golf Pride is the number one grip in golf.'
            ),
            'services' => array('Site Updates'),
            'logo' => 'logo-client-golf-pride.svg',
            'stats' => array(
                'Tour Players Using' => array(
                    'value' => 80,
                    'unit' => 'percentage',
                ),
                'Total Tour Wins' => 1138
                ),
            'link' => 'https://www.golfpride.com'
        ),

        array(
            'title' => 'Buckner Companies',
            'subtitle' => 'Leader in Heavylift Cranes and Steel Erection',
            'description' => array(
                'For decades Buckner Companies has been the driving force behind some of the largest projects around the country, such as the Arthur Ashe Stadium, Stennis Space Center Test Stands, Boeing’s Charleston, SC manufacturing facility, Bob Cats Arena, and many more prominent projects.'
            ),
            'services' => array('Site Updates', 'New Feature Additions'),
            'logo' => 'logo-client-buckner.svg',
            'stats' => array(
                'Heavylift Cranes' => 20,
                'Years of Experience' => 70
                ),
            'link' => 'https://www.bucknercompanies.com'
        )

    );

    // Provide an SVG file name and it will return the data as text
    function get_svg($key) {
	    $file = 'library/svg/'.$key.'.svg';
	    return (file_exists($file) ? file_get_contents($file) : '');
    }

?>
