<?php

    // Initialize our form data
    $form = array(
        'input' => array(),
        'errors' => array(),
        'required' => array(
            array('field' => 'user_name', 'error' => 'Please provide your name.'),
            array('field' => 'user_email', 'error' => 'Please provide your email address.')
        ),
        'email' => array(
            'to' => 'contact@danielstrong.io',
            'from_name' => 'Daniel Strong',
            'from_email' => 'noreply@danielstrong.io',
            'url' => '?thank_you=true'
        )
    );

    // This functions cycles through the $_POST data and sanitizes it. It also works for arrays of data
    function sanitize_form($input) {
        // If it's an array break it apart and resubmit to the function
        if (is_array($input)) {
            $array = [];
            foreach ($input as $key => $val) {
                $array[$key] = sanitize_form($val);
            }
            return $array;
        } else {
            return htmlspecialchars(stripslashes(trim($input)), ENT_QUOTES);
        }
    }

    // This function performs validation checks for required fields, and human testing
    function form_is_valid() {
        global $form;

        // Cycle through our required fields and make sure they all have data
        foreach ($form['required'] as $required) {
            if (empty($form['input'][$required['field']])) {
                $form['status'] = 'error';
                $form['errors'][] = $required['error'];
                return false;
            }
        }

        // Validate email address
        if (!filter_var($form['input']['user_email'], FILTER_VALIDATE_EMAIL)) {
            $form['status'] = 'error';
            $form['errors'][] = 'Please provide a valid email address.';
            return false;
        }

        // Test for humans
        if (!empty($form['input']['breathing'])) {
            $form['status'] = 'error';
            return false;
        }

        return true;
    }

    // If there is any $_POST data, let's get this party started
    if (!empty($_POST)) {

        // Sanitize our inputs and store in our $form
        $form['input'] = sanitize_form($_POST);

        // If our data passes validation, then proceed to mail processing
        if (form_is_valid()) {

            // Set up our form for mailing
            $to = array(
                $form['email']['to'],
                $form['input']['user_email']
            );
            $from_name = $form['email']['from_name'];
            $from_email = $form['email']['from_email'];

            $subject = "Portfolio Form Submission";

            // Build html email from template file
            $body = file_get_contents(realpath(__DIR__).'/library/email/template.html');
            $body = str_replace(
                array(
                    '{{user_name}}',
                    '{{user_email}}',
                    '{{user_comments}}',
                    '{{from_address}}',
                    '{{year}}'
                ),
                array(
                    $form['input']['user_name'],
                    $form['input']['user_email'],
                    $form['input']['user_comments'],
                    $form['email']['from_email'],
                    date('Y')
                ),
                $body
            );

            $header = 'From: ' . $from_name . ' <' . $from_email . '>' . "\r\n";
            $header .= "Reply-To: " . $from_email . "\r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $header .= "X-Mailer: PHP/" . phpversion();

            // Loop through our list of recipients and mail
            foreach ($to as $recipient) {
                try {
                    mail($recipient, $subject, $body, $header);
                } catch (Exception $e) { }
            }
            
            // Redirect user to the thank you url
            header("Location:" . $form['email']['url']);
            exit;

        }

    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="theme-color" content="#289784">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Daniel Strong // Front-End Developer</title>

    <link rel="stylesheet" href="library/css/main.css?v=<?=filemtime('library/css/main.css')?>">
    <link rel="icon" type="image/png" href="library/img/favicon.png">

    <!-- Hotjar Tracking Code for www.danielstrong.io -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:817551,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>

<body>
